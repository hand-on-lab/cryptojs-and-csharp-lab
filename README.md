# cryptoJS-and-csharp-lab

Lab for demo how to integrate with cryptoJS as Client and C# as Server

## client

[crypto-js](https://www.npmjs.com/package/crypto-js)

## server

[dotnet script](https://github.com/filipw/dotnet-script)

## Ref

- [System.Security.Cryptography - Aes](https://docs.microsoft.com/en-us/dotnet/api/system.security.cryptography.aes?view=net-5.0)
- [CryptoJS](https://code.google.com/archive/p/crypto-js)
- [关于CryptoJS AES后输出hex的16进制和Base64的问题。](https://www.jianshu.com/p/8b8c53907f31)
- [Compatible AES encryption and decryption for C# and javascript](https://stackoverflow.com/questions/47891104/compatible-aes-encryption-and-decryption-for-c-sharp-and-javascript)
- [Encrypt in javascript and decrypt in C# with AES algorithm](https://www.py4u.net/discuss/348126)
- [Crypto-js returns different values every time it's run when using AES](https://stackoverflow.com/questions/24455810/crypto-js-returns-different-values-every-time-its-run-when-using-aes)