using System;
using System.Security.Cryptography;
using System.Text;
using System.IO;

public class Program
{
	public static void Main()
	{
		try
        {		
          	const string key = "DU53q=n5hbaZJw&@";
          	const string iv = "1000000000000000";
			
			var encrypted = EncryptByAES("90622ea055dbcf176f8da99f3465f7a6", key, iv);
         	Console.WriteLine($"encrypted Result:{encrypted}");
          	var decrypted = DecryptByAES(encrypted, key, iv);
         	Console.WriteLine($"decrypted Result:{decrypted}");
			
			
          	const string cipherText = "FA8BD7AE067CF2309589AFBB89B3D31D191606352C51BDB1B17502C081EB84386F3570CB98DBEBC8E3A9DE3CC4F57A82";
			var decrypted_cipherText = DecryptByAES(cipherText, key, iv);
         	Console.WriteLine($"decrypted_cipherText Result:{decrypted_cipherText}");
        }
        catch (Exception e)
        {
        	Console.WriteLine(e);
        }
	} 
	
	public static string MD5Hash(string input)
    {
        using (var md5 = MD5.Create())
        {
            var result = md5.ComputeHash(Encoding.ASCII.GetBytes(input));
            var strResult = BitConverter.ToString(result);
            return strResult.Replace("-", "");
        }
    }
	
	
    /// <summary>  
    /// AES加密演算法  
    /// </summary>  
    /// <param name="input">明文字串</param>  
    /// <param name="key">金鑰（32位）</param>  
    /// <returns>字串</returns>  
    public static string EncryptByAES(string input, string key, string iv)
    {
        byte[] keyBytes = Encoding.UTF8.GetBytes(key);
        using (AesCryptoServiceProvider aesAlg = new AesCryptoServiceProvider())
        {
            aesAlg.Key = keyBytes;
            aesAlg.IV = Encoding.UTF8.GetBytes(iv);

            ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);
            using (MemoryStream msEncrypt = new MemoryStream())
            {
                using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                {
                    using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                    {
                        swEncrypt.Write(input);
                    }
                    byte[] bytes = msEncrypt.ToArray();
                    return ByteArrayToHexString(bytes);
                }
            }
        }
    }

    /// <summary>  
    /// AES解密  
    /// </summary>  
    /// <param name="input">密文位元組陣列</param>  
    /// <param name="key">金鑰（32位）</param>  
    /// <returns>返回解密後的字串</returns>  
    public static string DecryptByAES(string input, string key, string iv)
    {
        byte[] inputBytes = HexStringToByteArray(input); 
        byte[] keyBytes = Encoding.UTF8.GetBytes(key);
        using (AesCryptoServiceProvider aesAlg = new AesCryptoServiceProvider())
        {
            aesAlg.Key = keyBytes;
            aesAlg.IV = Encoding.UTF8.GetBytes(iv);

            ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);
            using (MemoryStream msEncrypt = new MemoryStream(inputBytes))
            {
                using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, decryptor, CryptoStreamMode.Read))
                {
                    using (StreamReader srEncrypt = new StreamReader(csEncrypt))
                    {
                        return srEncrypt.ReadToEnd();
                    }
                }
            }
        }
    }

    /// <summary>
    /// 將指定的16進位制字串轉換為byte陣列
    /// </summary>
    /// <param name="s">16進位制字串(如：“7F 2C 4A”或“7F2C4A”都可以)</param>
    /// <returns>16進位制字串對應的byte陣列</returns>
    public static byte[] HexStringToByteArray(string s)
    {
        s = s.Replace(" ", "");
        byte[] buffer = new byte[s.Length / 2];
        for (int i = 0; i < s.Length; i += 2){
            buffer[i / 2] = (byte)Convert.ToByte(s.Substring(i, 2), 16);
		}
        return buffer;
    }

    /// <summary>
    /// 將一個byte陣列轉換成一個格式化的16進位制字串
    /// </summary>
    /// <param name="data">byte陣列</param>
    /// <returns>格式化的16進位制字串</returns>
    public static string ByteArrayToHexString(byte[] data)
    {
        StringBuilder sb = new StringBuilder(data.Length * 3);
        foreach (byte b in data)
        {
            //16進位制數字
            sb.Append(Convert.ToString(b, 16).PadLeft(2, '0'));
            //16進位制數字之間以空格隔開
            //sb.Append(Convert.ToString(b, 16).PadLeft(2, '0').PadRight(3, ' '));
        }
        return sb.ToString().ToUpper();
    }

	public static string Decrypt(string ciphertext, string key, string iv)
    {
		using var aes = Aes.Create();
   		aes.Key = Encoding.UTF8.GetBytes(key);
       	aes.IV = Encoding.UTF8.GetBytes(iv);
        aes.Mode = CipherMode.CBC;
        aes.Padding = PaddingMode.PKCS7;
        using var transform = aes.CreateDecryptor();
        var bytes = Convert.FromBase64String(ciphertext);
        var result = transform.TransformFinalBlock(bytes, 0, bytes.Length);
        return Encoding.UTF8.GetString(result);
   	}
}