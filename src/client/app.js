const CryptoJS = require('crypto-js')

const key = CryptoJS.enc.Utf8.parse('DU53q=n5hbaZJw&@')
const iv = CryptoJS.enc.Utf8.parse('1000000000000000')
const opts = {
  iv,
  mode: CryptoJS.mode.CBC,
  padding: CryptoJS.pad.Pkcs7
}

const encryptInput = CryptoJS.MD5('pcsupport0001tn1tdp').toString()
const encryptInstance = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(encryptInput), key, opts)
const cipher = encryptInstance.ciphertext.toString().toUpperCase() //CryptoJS.AES.encrypt(raw, key, opts).toString()
const decryptInput = CryptoJS.enc.Base64.stringify(CryptoJS.enc.Hex.parse(cipher))
const decryptResult = CryptoJS.AES.decrypt(decryptInput, key, opts).toString(CryptoJS.enc.Utf8).toString()

//Decode from text    
// let cipherParams = CryptoJS.lib.CipherParams.create({
//   ciphertext: CryptoJS.enc.Base64.parse(cipher )
// });
// let decryptedFromText = CryptoJS.AES.decrypt(cipherParams, key, { iv: iv});
// console.log(decryptedFromText.toString(CryptoJS.enc.Utf8));


let resultObj = {
  "plain text": encryptInput,
  "encrypt result": cipher,
  "decrypt result": decryptResult
}

console.log(resultObj);